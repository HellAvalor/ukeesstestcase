package com.andreykaraman.ukeesstestcase.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import com.andreykaraman.ukeesstestcase.R;
import com.andreykaraman.ukeesstestcase.model.Event;
import com.andreykaraman.ukeesstestcase.model.Person;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Andrew on 12.08.2015.
 */
public class PersonAdapter extends BaseExpandableListAdapter {

    private ArrayList<Person> persons;
    private Context context;

    public PersonAdapter(ArrayList<Person> persons, Context context) {
        this.persons = persons;
        this.context = context;
    }

    @Override
    public int getGroupCount() {
        return persons.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        int count = 1;
        if (persons.get(groupPosition).getDeath() != null)
            count++;
        return count;
    }

    @Override
    public Person getGroup(int groupPosition) {
        return persons.get(groupPosition);
    }

    @Override
    public Event getChild(int groupPosition, int childPosition) {
        return childPosition == 0 ? getGroup(groupPosition).getBirth() : getGroup(groupPosition).getDeath();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        View view = convertView;
        ParentViewHolder holder;

        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService
                    (Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.pesron_info, parent, false);
            holder = getParentHolder(view);
            view.setTag(holder);
        } else {
            holder = (ParentViewHolder) view.getTag();
        }

        Person person = getGroup(groupPosition);
        Picasso.with(context).load(person.getPortrait()).into(holder.photo);
        holder.name.setText(person.getName());
        holder.gender.setText(person.getSex());
        ExpandableListView mExpandableListView = (ExpandableListView) parent;
        mExpandableListView.expandGroup(groupPosition);
        return view;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        View view = convertView;
        ChildViewHolder holder;

        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService
                    (Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.event_info, parent, false);
            holder = getChildHolder(view);
            view.setTag(holder);
        } else {
            holder = (ChildViewHolder) view.getTag();
        }

        Event event = getChild(groupPosition, childPosition);

        holder.eventName.setText(childPosition == 0 ? "Birth" : "Death");
        holder.locName.setText(event.getName());
        holder.lat.setText(event.getLatitude());
        holder.lon.setText(event.getLongitude());
        holder.date.setText(event.getDate());
        return view;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

    private ChildViewHolder getChildHolder(View view) {
        ChildViewHolder holder = new ChildViewHolder();
        holder.eventName = (TextView) view.findViewById(R.id.eventName);
        holder.locName = (TextView) view.findViewById(R.id.location);
        holder.lat = (TextView) view.findViewById(R.id.lat);
        holder.lon = (TextView) view.findViewById(R.id.longtitude);
        holder.date = (TextView) view.findViewById(R.id.date);
        return holder;
    }

    class ChildViewHolder {
        TextView eventName;
        TextView locName;
        TextView lat;
        TextView lon;
        TextView date;
    }

    private ParentViewHolder getParentHolder(View view) {
        ParentViewHolder holder = new ParentViewHolder();
        holder.photo = (ImageView) view.findViewById(R.id.photo);
        holder.name = (TextView) view.findViewById(R.id.name);
        holder.gender = (TextView) view.findViewById(R.id.gender);
        return holder;
    }

    class ParentViewHolder {
        ImageView photo;
        TextView name;
        TextView gender;
    }
}
