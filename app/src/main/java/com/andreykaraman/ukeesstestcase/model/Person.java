package com.andreykaraman.ukeesstestcase.model;

import com.google.gson.annotations.Expose;

/**
 * Created by Andrew on 12.08.2015.
 */
public class Person {

    @Expose
    private String Name;
    @Expose
    private String Portrait;

    @Expose
    private String Sex;

    @Expose
    private Event Birth;

    @Expose
    private Event Death;

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getPortrait() {
        return Portrait;
    }

    public void setPortrait(String portrait) {
        Portrait = portrait;
    }

    public String getSex() {
        return Sex;
    }

    public void setSex(String sex) {
        Sex = sex;
    }

    public Event getBirth() {
        return Birth;
    }

    public void setBirth(Event birth) {
        Birth = birth;
    }

    public Event getDeath() {
        return Death;
    }

    public void setDeath(Event death) {
        Death = death;
    }
}
