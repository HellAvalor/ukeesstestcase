package com.andreykaraman.ukeesstestcase.model;

import com.google.gson.annotations.Expose;

/**
 * Created by Andrew on 12.08.2015.
 */
public class Event {

    @Expose
    private String Date;
    @Expose
    private String Name;
    @Expose
    private String Latitude;
    @Expose
    private String Longitude;

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getLatitude() {
        return Latitude;
    }

    public void setLatitude(String latitude) {
        Latitude = latitude;
    }

    public String getLongitude() {
        return Longitude;
    }

    public void setLongitude(String longitude) {
        Longitude = longitude;
    }
}
