package com.andreykaraman.ukeesstestcase.model;

import com.google.gson.annotations.Expose;

import java.util.ArrayList;

/**
 * Created by Andrew on 12.08.2015.
 */
public class PersonsList {
    @Expose
    private ArrayList<Person> Persons;

    public ArrayList<Person> getPersons() {
        return Persons;
    }

    public void setPersons(ArrayList<Person> persons) {
        Persons = persons;
    }
}
