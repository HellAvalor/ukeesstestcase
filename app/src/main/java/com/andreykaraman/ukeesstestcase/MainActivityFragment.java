package com.andreykaraman.ukeesstestcase;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import com.andreykaraman.ukeesstestcase.model.Person;
import com.andreykaraman.ukeesstestcase.model.PersonsList;
import com.andreykaraman.ukeesstestcase.ui.PersonAdapter;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;


/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment {

    public MainActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_main, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ArrayList<Person> personList;
        Gson gson = new GsonBuilder().create();
        personList = gson.fromJson(loadJSONFromAsset(), PersonsList.class).getPersons();
        ExpandableListView listView = (ExpandableListView)view.findViewById(R.id.mainList);
        listView.setAdapter(new PersonAdapter(personList, getActivity()));
    }

    private String loadJSONFromAsset() {
        String json;
        try {
            InputStream is = getActivity().getAssets().open("AncestryCodingExercise.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

}